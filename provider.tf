provider "google" {
  project     = "project-test-to"
  credentials = "${file("./creds/serviceaccount.json")}"
  region      = "europe-west1"
}

provider "google-beta" {
  project     = "project-test-to"
  region      = "europe-west1"
}
