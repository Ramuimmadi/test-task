resource "google_container_node_pool" "private_np" {
  name               = "cluster-np"
  location           = "europe-west1"
  cluster            = "${google_container_cluster.cluster_1.name}"
  initial_node_count = 1

  lifecycle {
    ignore_changes = ["node_count"]
  }

  autoscaling {
    min_node_count = 1
    max_node_count = 2
  }

  management {
    auto_repair  = true
    auto_upgrade = true
  }

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/pubsub",
    ]

    tags = ["private-gke"]
  }
}

resource "google_container_cluster" "cluster_1" {
  name                     = "cluster-1"
  location                   = "europe-west1"
  remove_default_node_pool = true

  private_cluster_config {
    enable_private_endpoint = false
    enable_private_nodes    = true
    master_ipv4_cidr_block  = "172.16.0.0/28"
  }

  ip_allocation_policy {
    create_subnetwork = true
  }

  lifecycle {
    ignore_changes = ["initial_node_count", "network_policy", "node_config", "node_pool"]
  }

  node_pool {
    name = "default-pool"
  }

  addons_config {
    http_load_balancing {
      disabled = false
    }

    horizontal_pod_autoscaling {
      disabled = false
    }
  }
  master_authorized_networks_config {
    cidr_blocks {
      cidr_block = "10.128.0.0/24"
      display_name = "test"
    }
   }

}

