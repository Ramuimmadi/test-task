terraform {
  backend "gcs" {
    bucket  = "ptto-terraform-state"
    prefix  = "testtask"
  }
}
